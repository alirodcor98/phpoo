<?php
    
    include_once('transporte.php');

    class nave extends transporte{

		private $destino;

		//declaracion de constructor
		public function __construct($nom,$vel,$com,$dest){
			//sobreescritura de constructor de la clase padre
			parent::__construct($nom,$vel,$com);
			$this->destino=$dest;
				
		}

		// declaracion de metodo
		public function resumenNave(){
			// sobreescribitura de metodo crear_ficha en la clse padre
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Destino:</td>
						<td>'. $this->destino.'</td>				
					</tr>';
			return $mensaje;
		}
	} 

?>