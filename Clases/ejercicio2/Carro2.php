<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $anioFabricacion;
	private $estadoVerificacion;

	//declaracion del método verificación
	public function verificacion(){

		if(intval($_POST['anioFabricacion']) < 1990){
			$this->estadoVerificacion = "No";
		}

		elseif(intval($_POST['anioFabricacion']) >= 1990 and intval($_POST['anioFabricacion']) <= 2010){
			$this->estadoVerificacion = "Revisión";
		}

		elseif(intval($_POST['anioFabricacion']) > 2010){
			$this->estadoVerificacion = "Sí";
		}
		
	}

	public function get_estadoVerificacion(){
		return $this->estadoVerificacion;
	}

}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();
$Carro1->verificacion();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->anioFabricacion=$_POST['anioFabricacion'];
}




